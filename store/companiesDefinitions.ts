import { defineStore } from 'pinia';

import { useContext } from '@nuxtjs/composition-api';
import { COMPANIES_DEFINITIONS } from '~/tools/types';

export const useStore = defineStore('companiesDefinitions', {
  state: () => ({
    data: {} as COMPANIES_DEFINITIONS,
  }),

  getters: {
    Industry: state => state.data.Industry,
    CompanySpecialization: state => state.data.CompanySpecialization,
    IndustryGroup: state => state.data.IndustryGroup,
    CompanySpecializationGroup: state => state.data.CompanySpecializationGroup,
  },
  actions: {
    async fetchDefinitions() {
      const axios = useContext().$axios;

      try {
        const { data } = await axios({
          url: `http://api-test.duotek.ru/definitions`,
          method: 'GET',
        });
        // console.log('data', data);
        this.data = data;
      } catch (error: any) {
        this.data = error.message;
      }
    },
  },
});
