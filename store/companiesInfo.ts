import { defineStore } from 'pinia';

import { useContext } from '@nuxtjs/composition-api';
import { COMPANIES_INFO } from '~/tools/types';

export const useStore = defineStore('companiesInfo', {
  state: () => ({
    data: {} as COMPANIES_INFO,
  }),

  actions: {
    async fetchInfo(id: string) {
      const axios = useContext().$axios;

      try {
        const { data } = await axios({
          url: `http://api-test.duotek.ru/companies/info?id=${id}`,
          method: 'GET',
        });
        this.data = data.data;
      } catch (error: any) {
        this.data = error.message;
      }
    },
  },
});
