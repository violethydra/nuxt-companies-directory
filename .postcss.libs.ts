import { resolve } from 'path';

const rootNoExt = ({ name, type }: { name: string; type: string }) =>
  resolve(process.cwd(), `assets/${type}/`, `${name}`);

export const libs = {
  'postcss-at-rules-variables': {},

  'postcss-nested': {},

  'postcss-mixins': {
    beforeEach: ['postcss-custom-properties', 'postcss-simple-vars'],
    afterEach: ['postcss-at-rules-variables', 'postcss-nested'],
    mixinsDir: rootNoExt({ name: 'mixins', type: 'pcss' }),
  },

  'postcss-for': {},
  'postcss-each': {
    afterEach: ['postcss-at-rules-variables', 'postcss-nested'],
    beforeEach: [],
  },

  'postcss-url': {},
  'postcss-import': {},

  'postcss-responsive-type': {},
  'postcss-color-mod-function': {},
  'postcss-hexrgba': {},

  'postcss-sorting': {
    'properties-order': [
      'content',
      'display',
      'background',
      'background-image',
      'background-color',
      'width',
      'height',
      'margin',
      'padding',
      'border',
      'position',
      'top',
      'right',
      'bottom',
      'left',
      'transform',
      'cursor',
      //
    ],
  },
  'postcss-pxtorem': {
    rootValue: 16,
    propList: ['*', '!border*'],
  },
  'postcss-calc': {},
  'postcss-gap-properties': {},
  'postcss-combine-duplicated-selectors': {},
};
