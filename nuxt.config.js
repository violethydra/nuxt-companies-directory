import { libs } from './.postcss.libs';

const { npm_package_name: npmName, npm_package_description: npmDesc } = process.env;
const profile = process.env.NODE_ENV === 'development' ? 'risky' : 'experimental';

export default {
  head: {
    title: npmName,
    htmlAttrs: {
      lang: 'ru',
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: `${npmName} description`,
        content: npmDesc,
      },
      { name: 'format-detection', content: 'telephone=no' },
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
  },
  loading: { color: '#030953' },
  telemetry: false,
  parallel: true,
  caches: true,
  generate: { interval: 2000 },

  css: [],

  plugins: [],

  components: true,

  buildModules: [
    '@nuxtjs/composition-api/module',
    '@nuxt/typescript-build',
    '@nuxtjs/stylelint-module',
    '@nuxt/postcss8',
    '@nuxtjs/svg',
    '@nuxtjs/google-fonts',
    'nuxt-build-optimisations',
    '@pinia/nuxt',
  ],

  googleFonts: {
    display: 'swap',
    families: {
      Rubik: [100, 300, 400, 700],
      Raleway: [100, 300, 400, 700],
      'IBM Plex Sans': [100, 300, 400, 700],
    },
  },

  buildOptimisations: { profile },

  modules: ['@nuxtjs/axios', '@nuxtjs/svg-sprite'],

  axios: {},

  build: {
    analyze: false,
    cssSourceMap: true,
    postcss: {
      plugins: {
        ...libs,
        cssnano: {
          preset: [
            'default',
            {
              svgo: false,
              cssDeclarationSorter: false,
              autoprefixer: false,
              calc: false,
              discardComments: { removeAll: true },
            },
          ],
        },
      },
      order: 'presetEnvAndCssnanoLast',
      preset: {
        stage: 2,
        autoprefixer: {
          grid: true,
          flex: true,
          overrideBrowserslist: ['last 3 versions'],
        },
      },
    },
  },
};
