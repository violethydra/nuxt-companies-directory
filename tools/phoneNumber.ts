import { computed } from '@nuxtjs/composition-api';

export const phoneNumber = ({ str, type }: { str: string; type: string }) => {
  const convert = (tel: string) => {
    if ((tel ?? false) && tel.length >= 7) {
      const n = tel.replace(/\s+/g, '');

      const pre = `${n.substring(0, 1)}`;
      const city = `${n.substring(1, 4)}`;
      const g1 = `${n.substring(4, 7)}`;
      const g2 = `${n.substring(7, 9)}`;
      const g3 = `${n.substring(9, 11)}`;

      return `+${pre} (${city}) ${g1}-${g2}-${g3}`;
    }
    return '-';
  };

  const convertedPhone = computed(() => (type === 'tel' ? convert(str) : str));

  return { convertedPhone };
};
