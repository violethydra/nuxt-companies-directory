import { Spread } from './spread';

interface ID_INDENT_TITLE {
  id: number;
  ident: string;
  title: string;
}

interface ID_INDENT_TITLE_ICON extends ID_INDENT_TITLE {
  icon: string;
}

export interface COMPANIES_INFO {
  id: number;
  ident: string;
  title: string;
  description_short: string;
  description_full: string;
  url: string;
  contact_email: string;
  contact_phone: string;
  contact_telegram: string;
  contact_whatsapp: string;
  contact_skype: string;
  staff: number;
  age: number;
  city_id: number;
  city: Spread<ID_INDENT_TITLE, { country_id: number }>;
  country_id: number;
  country: ID_INDENT_TITLE;
  user_id: number;
  industries: Spread<
    ID_INDENT_TITLE,
    {
      icon: string;
      industry_group_id: number;
    }
  >[];
  companySpecializations: Spread<
    ID_INDENT_TITLE_ICON,
    {
      company_specialization_group_id: number;
    }
  >[];
  picture: string;
  created_at: number;
  updated_at: number;
}

export interface COMPANIES_DEFINITIONS {
  Industry: Spread<
    ID_INDENT_TITLE_ICON,
    {
      industry_group_id: number;
      industryGroup: ID_INDENT_TITLE_ICON;
    }
  >[];

  CompanySpecialization: Spread<
    ID_INDENT_TITLE_ICON,
    {
      company_specialization_group_id: number;
      companySpecializationGroup: ID_INDENT_TITLE_ICON;
    }
  >[];

  IndustryGroup: ID_INDENT_TITLE_ICON[];
  CompanySpecializationGroup: ID_INDENT_TITLE_ICON[];
}
