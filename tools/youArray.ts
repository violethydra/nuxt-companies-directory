/**
 * Is Array and not Empty
 * @param arr []
 * @returns [] or false
 */
export const youArray = (arr: any) => {
  if (Array.isArray(arr) && !!arr.length) return arr;
  else false;
};
